using UnityEngine;
using System;

public static class CoordinateConverter
{
    public static Vector2? WorldSpaceToClipSpace(Vector3 point, Matrix4x4 view, Matrix4x4 projection)
    {
        Vector4 clipSpacePosition = projection * view * new Vector4(point.x, point.y, point.z, 1.0f);

        if (clipSpacePosition.w == 0.0f)
            return null;

        clipSpacePosition /= clipSpacePosition.w;

        return new Vector2(clipSpacePosition.x, clipSpacePosition.y);
    }
    public static Vector3? ClipSpaceToWorldSpace(Vector2 point, Matrix4x4 inverseViewProjection)
    {
        Vector4 homogeneousPoint = new Vector4(point.x, point.y, -1.0f, 1.0f);
        Vector4 worldSpacePosition = inverseViewProjection * homogeneousPoint;

        if (worldSpacePosition.w == 0.0f)
            return null;

        worldSpacePosition /= worldSpacePosition.w;

        return new Vector3(worldSpacePosition.x, worldSpacePosition.y, worldSpacePosition.z);
    }


}
