using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class VirtualEmitter : MonoBehaviour
{
    public Transform character;
    public Transform camera;
    public Vector3 VirtualEmitterPosition;
    public AK.Wwise.Event wwiseEvent;
    GameObject VE;

    public bool isVirtualized = true;
    public bool ScreenSpace = false;

    // Start is called before the first frame update
    void Start()
    {
        VE = new GameObject("Virtual Object");
        VE.AddComponent<Transform>();
        
        wwiseEvent.Post(VE);
    }

    // Update is called once per frame
    void Update()
    {
        VirtualEmitterPosition = GetVirtualEmitterPosition();



        if (isVirtualized)
        {
            VE.transform.position = VirtualEmitterPosition;
        }
        else VE.transform.position = gameObject.transform.position;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            isVirtualized = true;
            AkSoundEngine.SetState("STATE_AttenuationType", "Virtual");
            ScreenSpace = false;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            isVirtualized = false;
            AkSoundEngine.SetState("STATE_AttenuationType", "Virtual");
            ScreenSpace = false;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            isVirtualized = false;
            AkSoundEngine.SetState("STATE_AttenuationType", "ScreenSpace");
            ScreenSpace = true;
        }
    }




    private void OnDrawGizmos()
    {
        if (VE == null) return; 
        if (isVirtualized)
        {
            Gizmos.color = Color.green;
        }
        else if (ScreenSpace)
        {
            Gizmos.color = Color.cyan;
        }
        else Gizmos.color = Color.red;
        
        Gizmos.DrawSphere(VE.transform.position, 0.5f);
    }


    Vector3 GetVirtualEmitterPosition()
    {
        float distance = Vector3.Distance(character.position, transform.position);
        Vector3 direction = (transform.position - camera.position).normalized;
        return camera.position + distance * direction;

    }
    private void OnDestroy()
    {
        AkSoundEngine.UnregisterGameObj(VE);
    }
}
