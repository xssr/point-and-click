using UnityEngine;

public class ControllerSwitcher : MonoBehaviour
{
    public GameObject thirdPersonController;
    public GameObject pointAndClickController;

    private Camera thirdPersonCamera;
    private Camera pointAndClickCamera;

    private MonoBehaviour[] thirdPersonComponents;
    private MonoBehaviour[] pointAndClickComponents;

    private void Start()
    {
        thirdPersonCamera = thirdPersonController.GetComponentInChildren<Camera>();
        pointAndClickCamera = pointAndClickController.GetComponentInChildren<Camera>();

        thirdPersonComponents = thirdPersonController.GetComponentsInChildren<MonoBehaviour>();
        pointAndClickComponents = pointAndClickController.GetComponentsInChildren<MonoBehaviour>();

        ActivateThirdPersonController();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActivateThirdPersonController();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActivatePointAndClickController();
        }
    }

    private void ActivateThirdPersonController()
    {
        foreach (var component in thirdPersonComponents)
        {
            component.enabled = true;
        }
        thirdPersonCamera.enabled = true;

        foreach (var component in pointAndClickComponents)
        {
            component.enabled = false;
        }
        pointAndClickCamera.enabled = false;
    }

    private void ActivatePointAndClickController()
    {
        foreach (var component in thirdPersonComponents)
        {
            component.enabled = false;
        }
        thirdPersonCamera.enabled = false;

        foreach (var component in pointAndClickComponents)
        {
            component.enabled = true;
        }
        pointAndClickCamera.enabled = true;
    }
}
