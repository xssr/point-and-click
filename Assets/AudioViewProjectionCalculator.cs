using UnityEngine;

public static class AudioViewProjectionCalculator
{
    // ���������� ��������������� ������� ���� � �������� ��� �����.
    public static Matrix4x4 GetAudioViewProjectionMatrix(Camera camera)
    {
        Matrix4x4 viewMatrix = camera.worldToCameraMatrix;
        Matrix4x4 projectionMatrix = camera.projectionMatrix;

        return projectionMatrix * viewMatrix;
    }

    public static Vector3 ScreenToWorld(Vector2 screenPosition, Camera camera)
    {
        return camera.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, camera.nearClipPlane));
    }
}
