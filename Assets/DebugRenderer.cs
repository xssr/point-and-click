using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class DebugRenderer : MonoBehaviour
{
    public bool drawCircle; 
    public int circleResolution = 16; 
    public float visualizationDistance = 10f; 
    public Color lineColor = Color.red;

    void OnDrawGizmos()
    {
        Vector3[] shapePoints;

        if (drawCircle)
        {
            shapePoints = GetCirclePoints(gameObject.transform.position, visualizationDistance, circleResolution);
        }
        else
        {
            shapePoints = GetRectanglePoints(gameObject.transform.position, visualizationDistance);
        }

        for (int i = 0; i < shapePoints.Length; i++)
        {
            Gizmos.DrawLine(shapePoints[i], shapePoints[(i + 1) % shapePoints.Length]);
        }
        Gizmos.color = lineColor;
    }

    Vector3[] GetCirclePoints(Vector3 center, float radius, int resolution)
    {
        Vector3[] points = new Vector3[resolution];

        float angleStep = 360f / resolution;
        for (int i = 0; i < resolution; i++)
        {
            float angle = Mathf.Deg2Rad * i * angleStep;
            points[i] = center + new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);
        }

        return points;
    }

    Vector3[] GetRectanglePoints(Vector3 center, float halfSideLength)
    {
        return new Vector3[]
        {
            center + new Vector3(-halfSideLength, 0, -halfSideLength),
            center + new Vector3(-halfSideLength, 0, halfSideLength),
            center + new Vector3(halfSideLength, 0, halfSideLength),
            center + new Vector3(halfSideLength, 0, -halfSideLength)
        };
    }
}
