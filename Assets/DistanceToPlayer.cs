using UnityEngine;

public class DistanceToPlayer : MonoBehaviour
{
    public Transform playerTransform;
    public AK.Wwise.Event wwiseevent;
    float distanceSquared;

    private void Start()
    {
        wwiseevent.Post(gameObject);
    }
    void Update()
    {
        Vector3 cubePosition = transform.position;
        Vector3 playerPosition = playerTransform.position;

        Vector2? cubeClipSpacePos = CoordinateConverter.WorldSpaceToClipSpace(cubePosition, Camera.main.worldToCameraMatrix, Camera.main.projectionMatrix);
        Vector2? playerClipSpacePos = CoordinateConverter.WorldSpaceToClipSpace(playerPosition, Camera.main.worldToCameraMatrix, Camera.main.projectionMatrix);

        if (cubeClipSpacePos.HasValue && playerClipSpacePos.HasValue)
        {
            distanceSquared = GetDistanceSquared(cubeClipSpacePos.Value, playerClipSpacePos.Value) * 10f;
            Debug.Log($"������� ���������� � ������������ ��������� ����� ����� � �������: {distanceSquared}");
        }
        AkSoundEngine.SetRTPCValue("RTPC_squared_distance", distanceSquared, gameObject);

    }

    private float GetDistanceSquared(Vector2 projectedPosition, Vector2 projectedAttenuationPosition)
    {
        float xDistance = projectedPosition.x - projectedAttenuationPosition.x;
        float yDistance = projectedPosition.y - projectedAttenuationPosition.y;
        Vector2 rescaledPosition = projectedPosition;

        rescaledPosition.x *= (xDistance >= 0.0f)   
            ? (20.0f - projectedAttenuationPosition.x) / 20.0f
            : (20.0f + projectedAttenuationPosition.x) / 20.0f;

        rescaledPosition.y *= (yDistance >= 0.0f)
            ? (20.0f - projectedAttenuationPosition.y) / 20.0f
            : (20.0f + projectedAttenuationPosition.y) / 20.0f;

        return Mathf.Max(rescaledPosition.x * rescaledPosition.x, rescaledPosition.y * rescaledPosition.y);
    }
}
